#!/usr/bin/env python
import thread
import socket  #create an endpoint communication
import sys
import subprocess

try:

    def Socket(sockdata,data):


        print sockdata
        data= "corp"
#created a simple loops for data exchange
        while len(data):
            data=sockdata.recv(4064)
            print "client sent: ",data

            sockdata.send(data)

        print "Closing the Connection......."

        sockdata.close()


#Create a TCP socket where 1st arg defines that the socket must be connected with ipv4 addr  and 2nd args define type of Socket(TCP)
    server=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#The process will be continue
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

#The address where the server wants to connect
    server.bind(("0.0.0.0", int(sys.argv[1])))

#initiates a connection for accepting the client
    server.listen(10)

    while True:

        print "Waiting for connection from client"

    #defines it is client side socket to which the server wants to connect with the IP & port & the client also will wait for recv &/+ send data
        client, addr = server.accept()

        print "Starting a New Thread...\n"

        thread.start_new_thread(Socket, (client, addr))

except IndexError:

    print "Enter the port No"

except KeyboardInterrupt:

    print "Kill The process"
